FROM ubuntu:18.04

RUN apt-get update

RUN apt-get install -y libapr1-dev libaprutil1-dev
RUN apt-get install -y libpcre3-dev
RUN apt-get install -y gcc
RUN apt-get install -y make

ARG APACHE_TAR_LINK=NONE
ADD ${APACHE_TAR_LINK} /root

WORKDIR /root
RUN tar -xf $(ls *.tar.gz)

RUN echo mv $(ls -d */)"*" . | /bin/bash
RUN ./configure --prefix=/etc/apache2
RUN make
RUN make install